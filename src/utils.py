import random
import string
import torch
from torch.autograd import Variable
from settings import CHARACTERS
import re

# Take random group of x characters from string 
def random_chunk(data, chunk_length):
    start = random.randint(0, len(data) - chunk_length)
    end = start + chunk_length + 1
    return data[start:end]

# Convert chunk of characters into a long tensor
def char_tensor(chunk):
    tensor = torch.zeros(len(chunk)).long()
    for c in range(len(chunk)):
        tensor[c] = CHARACTERS.index(chunk[c])

    return Variable(tensor)

# Pair an input with a target according to chunk
# Ex: chunk: 'abc', input: 'ab', target: 'bc'
def random_training_set(data, chunk_length):
    chunk = random_chunk(data, chunk_length)
    input_tensor = char_tensor(chunk[:-1])
    target_tensor = char_tensor(chunk[1:])
    return input_tensor, target_tensor


def readAmazonReviewFile(file):
    lines = file.readlines()
    ret = ""
    i = 0
    for line in lines:
        # match = re.search(r'[0-9]+\t[0-9]+\t\[0-9].[0-9] (.+)', line)
        # match = re.search(r'(.+)', line)
        # ret += match.group(1)
        ret += " ".join(line.split()[3:])
        ret += "\n"
        i += 1
        if i == 10000:
            break
    return ret
